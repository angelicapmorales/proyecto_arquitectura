## Installation

1. `pip install -r requirements.txt`
2. `python manage.py migrate`

## Running the Project

1. `python manage.py runserver`
2. `sudo docker run -p 5672:5672 --name rabbitmq_car4 rabbitmq:3`
3. `celery -A search_tech_project worker --loglevel=INFO`
4. `celery -A search_tech_project beat -l info`

## Testing

1. `coverage erase`
2. `coverage run manage.py test`
3. `coverage report`
4. `coverage html`
5. Open the `htmlcov/index.html` file report on your browser. 


## Description
This project do a web-scrapping to alkosto's web page in the laptops category.
The api content shows all the laptops with their specifications like a: name, ram
storage, ram, processors, etc.



