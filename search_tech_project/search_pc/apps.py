from django.apps import AppConfig


class SearchPcConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'search_pc'
