import json
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status

from search_pc.models import Computer
from search_pc.tasks import scrapper
from rest_framework.test import APITestCase


class ScrappingTest(TestCase):
    """
    This test is done in order to know that the scrapper returns a value other than None
    """

    def test_get_scrap_data(self):
        response = scrapper()
        self.assertIsNotNone(response)


class SearchPcTest(APITestCase):

    """
    To check rest methods such as GET, POST, PUT, DELETE and user authentication.
    """

    url = "http://127.0.0.1:8000/computers/"
    url1 = "http://127.0.0.1:8000/computers/1/"

    def setUp(self):
        """
        Data and global variables of the tests.

        Create and login to a user.
        """
        self.computer_data = {
            "name": "ComputerTest",
            "brand": "BrandTest",
            "link": "https://www.alkosto.com/computador-portatil-huawei-134-pulgadas-matebook",
            "ram": 2,
            "storage": 512,
            "processor": "Intel Core I7",
            "price": 5999900,
        }

        user = User.objects.create_superuser(username='admin', email='admin@gmail.com', password='lsv12345')
        self.client.login(username='admin', password='lsv12345')

    def test_get_pcs(self):
        """
        This test checks that the data is obtained correctly and that it is not empty.

        """
        Computer.objects.create(**self.computer_data)
        response = self.client.get(self.url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(len(result.get("results")), 1)

    def test_get_pcs_empty(self):
        """
        This test verifies that it gets working even when there is no data to display.

        """
        response = self.client.get(self.url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(len(result.get("results")), 0)

    def test_post_pc(self):
        """
        Check that the post method enters the data correctly, by checking the generated response .
        """
        response = self.client.post(self.url, self.computer_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        result = json.loads(response.content)
        name_response = result.get('name')
        self.assertEqual(name_response, self.computer_data.get('name'))

    def test_put_pc(self):
        """
        Check that the put method edit the data of a record correctly, by checking the generated response.
        """
        computer_data2 = {
            "name": "ComputerGamer",
            "brand": "BrandTest",
            "link": "https://www.alkosto.com/computador-portatil-huawei-134-pulgadas-matebook",
            "ram": 2,
            "storage": 512,
            "processor": "Intel Core I7",
            "price": 5999900,
        }

        response = self.client.post(self.url, self.computer_data, format="json")
        response_put = self.client.put(self.url1, computer_data2, format="json")
        self.assertEqual(response_put.status_code, status.HTTP_200_OK)
        result = json.loads(response_put.content)
        name_response_put = result.get('name')
        self.assertEqual(name_response_put, computer_data2.get('name'))

    def test_delete_pc(self):
        """
        Check that the put method delete a record correctly, by checking the generated response.
        """
        response = self.client.post(self.url, self.computer_data, format="json")
        response_delete = self.client.delete(self.url1, format="json")
        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)

    def test_login_invalid(self):
        """
        This test is done in order to verify user authentication through username and password.

        """
        response_login = self.client.login(username='luis', password='luis')
        self.assertEqual(response_login, False)

    def test_logout(self):
        """This test is done in order to verify the user's session closure,
        noting that you can only create, update, delete and access the data if you are logged in."""

        response = self.client.post(self.url, self.computer_data, format="json")
        self.client.logout()
        response_get = self.client.get(self.url1, format="json")
        self.assertEqual(response_get.status_code, status.HTTP_403_FORBIDDEN)


