from django.db import models


# Create your models here.

class Computer(models.Model):
    name = models.TextField(blank=False, null=False)
    brand = models.CharField(max_length=50, blank=False, null=False, default="")
    link = models.TextField(blank=False, null=False, unique=True)
    ram = models.IntegerField(blank=False, null=False)
    storage = models.IntegerField(blank=False, null=False)
    processor = models.CharField(max_length=200)
    price = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return self.name
