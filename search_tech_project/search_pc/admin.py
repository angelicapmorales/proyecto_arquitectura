from django.contrib import admin

# Register your models here.
from search_pc.models import Computer


@admin.register(Computer)
class ComputerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'brand', 'link', 'ram', 'storage', 'processor', 'price')
    search_fields = ('brand', 'processor')
    list_filter = ('brand', 'ram', 'storage', 'processor')

