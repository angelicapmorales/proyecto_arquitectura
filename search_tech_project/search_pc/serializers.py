from rest_framework import serializers

from search_pc.models import Computer


class ComputerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Computer
        fields = ['id','name', 'brand','link', 'ram', 'storage','processor','price']


