# Generated by Django 4.0.4 on 2022-04-26 22:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('search_pc', '0002_computer_brand'),
    ]

    operations = [
        migrations.AlterField(
            model_name='computer',
            name='link',
            field=models.TextField(unique=True),
        ),
        migrations.AlterField(
            model_name='computer',
            name='name',
            field=models.TextField(unique=True),
        ),
    ]
