from search_tech_project.celery import app
from bs4 import BeautifulSoup
import requests
import re
from search_pc.models import Computer


@app.task
def scrapper():
    """
    This function performs the web-scrapping to the page of the company Alkosto Hiper Ahorro,
    obtaining data from portable computers, data such as:
            name = Name of the laptop
            brand = computer brand
            link = product size link
            ram = RAM memory of the laptop
            storage = Storage capacity of the laptop
            processor = task processing device
            price = Price or purchase value of the laptop.
    """
    url = f"https://www.alkosto.com/computadores-tablet/computadores-portatiles/c/BI_104_ALKOS"
    page = requests.get(url).text
    doc = BeautifulSoup(page, "html.parser")

    tags_nav_ul = doc.find(class_="pagination--buttons")
    pages = int(tags_nav_ul.ul['data-numbers'])


    parent_filters = doc.find(class_="category-page plp plp--list")
    parent_brands_text = (parent_filters.find(string=re.compile('Marca')))
    parent_brands = parent_brands_text.find_parent(class_="facet__values js-facet-form")
    brands_tags = parent_brands.find_all(class_="facet__value__name")
    brands = []
    for brand_tag in brands_tags:
        brands.append(brand_tag.string)

    computer = {}
    count = 0
    computer_data = []
    for page in range(1, pages + 1):
        url = f"https://www.alkosto.com/computadores-tablet/computadores-portatiles/c/BI_104_ALKOS?page={page}"
        single_page = requests.get(url).text
        doc = BeautifulSoup(single_page, "html.parser")
        div = doc.find(class_="product__listing product__list")
        items = []
        pcs = div.find_all(string=re.compile('Computador'))
        macs = div.find_all(string=re.compile('MacBook'))
        dirty_items = pcs + macs

        for dirty_item in dirty_items:
            if dirty_item.startswith('Computador') or dirty_item.startswith('MacBook'):
                items.append(dirty_item)

        for item in items:
            count += 1
            parent = item.parent
            link = None
            if parent.name == "a":
                link = "https://www.alkosto.com" + parent['href']

            parent_2 = item.find_parent(class_="product__list--item product__list--alkosto")

            price_string = str(parent_2.find(class_="price").string)

            price = int((price_string).replace("$", "").replace(".", ""))

            parent_3 = item.find_parent(class_="product__information")

            specifications_set = parent_3.find_all(class_="item--value")

            if len(specifications_set) < 3:
                ram = 0
                store = 0
                processor = 'Does not apply'
            else:
                ram_string = str(specifications_set[0].string)
                ram = int((ram_string.replace(" GB", "")))

                store_string = str(specifications_set[1].string)
                store = int((store_string.replace(" GB", "")))

                processor = str(specifications_set[2].string)

            name = str(item.string).split("-")[-0]
            bad_names = ['AMD', 'Intel', 'Chip']
            for bad in bad_names:
                if bad in name:
                    name = name.split(f'{bad}')[-0]
            brand = 'None'
            for b in brands:
                if b in name:
                    brand = b
                elif (name.startswith('MacBook')):
                    brand = 'Apple'

            computer_object = Computer.objects.update_or_create(
                name = name,
                brand = brand,
                link = link,
                ram = ram,
                storage = store,
                processor = processor,
                price=price
            )
            computer = {
                'name':name,
                'brand':brand,
                'link':link,
                'ram':ram,
                'storage':store,
                'processor':processor,
                'price':price
            }

            computer_data.append(computer)

    return computer_data

