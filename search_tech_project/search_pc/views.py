from rest_framework import viewsets, mixins
from rest_framework import permissions

from search_pc.models import Computer
from search_pc.serializers import ComputerSerializer


class ComputerViewSet(viewsets.ModelViewSet, mixins.UpdateModelMixin):
    """
    API endpoint that allows COMPUTERS to be viewed or edited.
    """
    queryset = Computer.objects.all().order_by('price')
    serializer_class = ComputerSerializer
    permission_classes = [permissions.IsAdminUser]
